import { defineMessages } from "react-intl";

export default defineMessages({
	main: {
		id: "navigation.main",
		defaultMessage: "Logo"
	},
	about: {
		id: "navigation.about",
		defaultMessage: "About Us"
	},
	estate: {
		id: "navigation.estate",
		defaultMessage: "Real estates"
	},
	user: {
		id: "navigation.user",
		defaultMessage: "My page"
	}
});
