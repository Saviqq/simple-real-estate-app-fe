import React from "react";
import { Switch, Route } from "react-router-dom";

import { MainPage, AboutPage, EstatePage, UserPage } from "../pages";
import {
	MAIN_ROUTE,
	ABOUT_ROUTE,
	ESTATE_ROUTE,
	USER_ROUTE
} from "../constants/routes";

const Content = () => (
	<Switch>
		<Route path={MAIN_ROUTE} component={MainPage} />
		<Route path={ABOUT_ROUTE} component={AboutPage} />
		<Route path={ESTATE_ROUTE} component={EstatePage} />
		<Route path={USER_ROUTE} component={UserPage} />
	</Switch>
);

export default Content;
