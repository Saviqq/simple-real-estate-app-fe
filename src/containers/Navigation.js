import React from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { Grid, Button } from "@material-ui/core";

import message from "./navigation.message";
import {
	MAIN_ROUTE,
	ABOUT_ROUTE,
	ESTATE_ROUTE,
	USER_ROUTE
} from "../constants/routes";

const Navigation = () => (
	<Grid container direction="row">
		<Link to={MAIN_ROUTE}>
			<Button>
				<FormattedMessage {...message.main} />
			</Button>
		</Link>
		<Link to={ABOUT_ROUTE}>
			<Button>
				<FormattedMessage {...message.about} />
			</Button>
		</Link>
		<Link to={ESTATE_ROUTE}>
			<Button>
				<FormattedMessage {...message.estate} />
			</Button>
		</Link>
		<Link to={USER_ROUTE}>
			<Button>
				<FormattedMessage {...message.user} />
			</Button>
		</Link>
	</Grid>
);

export default Navigation;
