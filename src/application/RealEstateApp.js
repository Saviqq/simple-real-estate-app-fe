import React from "react";
import { IntlProvider } from "react-intl";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";

import Main from "./Main";
import configureStore from "../state/store";

const reduxStore = configureStore();

const RealEsateApp = () => (
	<IntlProvider locale="en">
		<BrowserRouter>
			<Provider store={reduxStore}>
				<Main />
			</Provider>
		</BrowserRouter>
	</IntlProvider>
);

export default RealEsateApp;
