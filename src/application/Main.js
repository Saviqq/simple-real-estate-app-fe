import React, { Fragment } from "react";
import Navigation from "../containers/Navigation";
import Content from "../containers/Content";

const Main = () => (
	<Fragment>
		<Navigation />
		<Content />
	</Fragment>
);

export default Main;
