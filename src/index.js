import React from "react";
import ReactDOM from "react-dom";
import RealEstateApp from "../src/application/RealEstateApp";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<RealEstateApp />, document.getElementById("root"));

registerServiceWorker();
