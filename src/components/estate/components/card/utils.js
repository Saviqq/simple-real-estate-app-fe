export const RealEstateTypes = {
	FLAT: "Flat",
	HOUSE: "House"
};

export const RealEstateDealType = {
	UNDEFINED: "UNDEFINED",
	HOT: "HOT",
	BEST: "BEST"
};

const planets = {
	mercury: {
		defaultImage: require("./assets/mercury.jpg")
	},
	venus: {
		defaultImage: require("./assets/venus.jpg")
	},
	earth: {
		defaultImage: require("./assets/earth.jpg")
	},
	mars: {
		defaultImage: require("./assets/mars.jpg")
	},
	jupiter: {
		defaultImage: require("./assets/jupiter.jpg")
	},
	saturn: {
		defaultImage: require("./assets/saturn.jpg")
	},
	uranus: {
		defaultImage: require("./assets/uranus.jpg")
	},
	neptune: {
		defaultImage: require("./assets/neptune.jpg")
	},
	pluto: {
		defaultImage: require("./assets/pluto.jpg")
	}
};

export function getDefaultImage(planet) {
	switch (planet) {
	case "Mercury":
		return planets.mercury.defaultImage;
	case "Venus":
		return planets.venus.defaultImage;
	case "Earth":
		return planets.earth.defaultImage;
	case "Mars":
		return planets.mars.defaultImage;
	case "Jupiter":
		return planets.jupiter.defaultImage;
	case "Saturn":
		return planets.saturn.defaultImage;
	case "Uranus":
		return planets.uranus.defaultImage;
	case "Neptune":
		return planets.neptune.defaultImage;
	case "Pluto":
		return planets.pluto.defaultImage;
	default:
		return require("./assets/no-image.png");
	}
}

export function getDefaultTitle(type, sector, planet) {
	return type + " at sector " + sector + " on " + planet;
}
