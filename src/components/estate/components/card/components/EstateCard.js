import React from "react";
import PropTypes from "prop-types";
import { Card, CardActionArea, CardMedia, CardContent, Typography } from "@material-ui/core";
import { Earth, MapMarker, CurrencyEur } from "mdi-material-ui";

import IconWithLabel from "./IconWithLabel";
import TypeIconWithLabel from "../containers/TypeIconWithLabel";
import EstateDetailsButton from "./EstateDetailsButton";

const styles = {
	card: {
		maxWidth: "375px"
	}
};

const EstateCard = ({title, imageURL, planet, sector, type, rooms, garage, price }) => (
	<Card style={styles.card}>
		<CardActionArea>
			<CardMedia
				component="img"
				style={{ objectFit: "cover" }}
				height="140"
				image={imageURL}
				title={title} />
		</CardActionArea>
		<CardContent>
			<Typography variant="title" gutterBottom> {title} </Typography>
			<IconWithLabel icon={<Earth />} label={planet} />
			<IconWithLabel icon={<MapMarker />} label={sector} />
			<TypeIconWithLabel type={type} rooms={rooms} garage={garage} /> 
			<IconWithLabel icon={<CurrencyEur />} label={price} />
			<EstateDetailsButton />
		</CardContent>
	</Card>
);

EstateCard.propTypes = {
	title: PropTypes.string.isRequired,
	imageURL: PropTypes.string.isRequired,
	planet: PropTypes.string.isRequired,
	sector: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	rooms: PropTypes.number.isRequired,
	garage: PropTypes.bool.isRequired,
	price: PropTypes.string
};

export default EstateCard;

