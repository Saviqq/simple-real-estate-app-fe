import React from "react";
import { Grid, Button, withStyles } from "@material-ui/core";
import { Email } from "mdi-material-ui";

const styles = theme => ({
	button: {
		margin: theme.spacing.unit
	},
	rightIcon: {
		marginLeft: theme.spacing.unit
	}
});

class EstateDetailsButton extends React.Component {
	handleClick = () => {
		alert("modal shows");
	};

	render() {
		const { classes } = this.props;
		return (
			<Grid container spacing={8} justify="center" alignItems="center">
				<Button
					variant="contained"
					color="primary"
					className={classes.button}
					onClick={() => this.handleClick()}
				>
					GET DETAILS
					<Email className={classes.rightIcon} />
				</Button>
			</Grid>
		);
	}
}

export default withStyles(styles)(EstateDetailsButton);
