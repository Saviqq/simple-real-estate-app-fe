import React from "react";
import PropTypes from "prop-types";
import { Grid, Typography } from "@material-ui/core";

const IconWithLabel = ({icon, label}) => (
	<Grid container spacing={8} direction="row" alignItems="center">
		<Grid item>
			{icon}
		</Grid> 
		<Grid item>
			<Typography>
				{label}
			</Typography>
		</Grid>
	</Grid>
);

IconWithLabel.propTypes = {
	icon: PropTypes.node.isRequired,
	label: PropTypes.string
};

export default IconWithLabel;
