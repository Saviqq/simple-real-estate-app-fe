import React from "react";
import PropTypes from "prop-types";
import EstateCard from "../components/EstateCard";

import { getDefaultImage, getDefaultTitle } from "../utils";

class EstateCardContainer extends React.Component {
	setTitle = () => {
		if (this.props.title === undefined) {
			return getDefaultTitle(
				this.props.type,
				this.props.sector,
				this.props.planet
			);
		} else {
			return this.props.title;
		}
	};

	setImage = () => {
		if (this.props.imageURL === undefined) {
			return getDefaultImage(this.props.planet);
		} else {
			return this.props.imageURL;
		}
	};

	render() {
		return (
			<EstateCard
				title={this.setTitle()}
				imageURL={this.setImage()}
				planet={this.props.planet}
				sector={this.props.sector}
				type={this.props.type}
				rooms={this.props.rooms}
				garage={this.props.garage}
				price={this.props.price}
				dealType={this.props.dealType}
			/>
		);
	}
}

EstateCardContainer.propTypes = {
	title: PropTypes.string,
	imageURL: PropTypes.string,
	planet: PropTypes.string.isRequired,
	sector: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	rooms: PropTypes.number.isRequired,
	garage: PropTypes.bool.isRequired,
	price: PropTypes.string,
	dealType: PropTypes.object
};

export default EstateCardContainer;
