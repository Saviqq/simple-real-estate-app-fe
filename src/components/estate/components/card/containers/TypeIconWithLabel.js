import React from "react";
import PropTypes from "prop-types";
import { Garage, Home, Domain } from "mdi-material-ui";

import IconWithLabel from "../components/IconWithLabel";
import { RealEstateTypes } from "../utils";

class TypeIconWithLabel extends React.Component {
	getIcons = () => {
		let icons = [];
		if (this.props.type === RealEstateTypes.FLAT) {
			icons.push(<Domain key={"domain"} />);
		} else {
			icons.push(<Home key={"home"} />);
		}

		if (this.props.garage) {
			icons.push(<Garage key={"garage"} />);
		}

		return icons;
	};

	getLabel = () => {
		let label = this.props.rooms;
		if (this.props.rooms > 1) {
			label += "-room ";
		} else label += "-rooms ";
		if (this.props.type === RealEstateTypes.FLAT) {
			label += "flat";
		} else {
			label += "house";
		}
		if (this.props.garage) {
			label += " with garage";
		}
		return label;
	};

	render() {
		return <IconWithLabel icon={this.getIcons()} label={this.getLabel()} />;
	}
}

TypeIconWithLabel.propTypes = {
	type: PropTypes.string.isRequired,
	rooms: PropTypes.number.isRequired,
	garage: PropTypes.bool.isRequired
};

export default TypeIconWithLabel;
