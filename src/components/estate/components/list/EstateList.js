import React, { Component } from "react";
import { Grid } from "@material-ui/core";

import EstateCardContainer from "../card/containers/EstateCardContainer";

export default class EstateList extends Component {
	render() {
		return (
			<Grid>
				<Grid container direction="row" justify="center" alignItems="center">
					<Grid item xs={12} sm={6} md={4} lg={3}>
						<EstateCardContainer
							planet={"Earth"}
							sector={"X257AR"}
							type={"House"}
							rooms={3}
							garage={true}
							price={"25.000"}
						/>
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						2{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						3{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						4{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						5{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						6{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						7{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						8{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						9{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						10
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						11{" "}
					</Grid>
					<Grid item xs={12} sm={6} md={4} lg={3}>
						{" "}
						12{" "}
					</Grid>
				</Grid>
			</Grid>
		);
	}
}
