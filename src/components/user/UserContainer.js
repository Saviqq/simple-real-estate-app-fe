import React, { Fragment } from "react";
import { connect } from "react-redux";

import { Login } from "./components/login";
import Dashboard from "./components/dashboard/Dashboard";

class UserContainer extends React.Component {
	render() {
		const { logged } = this.props;
		return <Fragment>{logged ? <Dashboard /> : <Login />}</Fragment>;
	}
}

const mapStateToProps = state => ({
	logged: state.userState.logged
});

export default connect(
	mapStateToProps,
	null
)(UserContainer);
