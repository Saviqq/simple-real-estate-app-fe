import React from "react";
import { connect } from "react-redux";

import userOperations from "../../../state/operations";
import Login from "../components/Login";

class LoginContainer extends React.Component {
	render() {
		return <Login onLogIn={this.props.logIn} />;
	}
}

const mapDispatchToProps = {
	logIn: userOperations.logIn
};

export default connect(
	null,
	mapDispatchToProps
)(LoginContainer);
