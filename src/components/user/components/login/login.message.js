import { defineMessages } from "react-intl";

const message = {
	username: "Username",
	password: "Password",
	formatted: defineMessages({
		login: {
			id: "login.login",
			defaultMessage: "Login"
		}
	})
};

export default message;
