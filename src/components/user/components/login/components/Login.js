import React from "react";
import PropTypes from "prop-types";
import { Button, Grid, TextField } from "@material-ui/core";
import { FormattedMessage } from "react-intl";

import message from "../login.message";

const Login = ({ onLogIn }) => (
	<Grid>
		<TextField
			label={message.username}
			margin="normal"
			variant="outlined"
			fullWidth
		/>
		<TextField
			label={message.password}
			margin="normal"
			variant="outlined"
			fullWidth
		/>
		<Button onClick={() => onLogIn()}>
			<FormattedMessage {...message.formatted.login} />
		</Button>
	</Grid>
);

Login.propTypes = {
	onLogIn: PropTypes.func.isRequired
};

export default Login;
