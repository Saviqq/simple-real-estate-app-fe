import React, { Fragment } from "react";
import { connect } from "react-redux";

import AdminUser from "./components/admin/AdminUser";
import RegularUser from "./components/regular/RegularUser";

class Dashboard extends React.Component {
	render() {
		return (
			<Fragment>
				{this.props.isAdmin ? <AdminUser /> : <RegularUser />}
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	isAdmin: state.userState.isAdmin
});

export default connect(
	mapStateToProps,
	null
)(Dashboard);
