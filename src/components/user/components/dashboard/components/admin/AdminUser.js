import React from "react";
import { Grid } from "@material-ui/core";

import EstateUploadPage from "./components/upload/EstateUploadPage";

const AdminUser = () => (
	<Grid>
		<EstateUploadPage />
	</Grid>
);

export default AdminUser;
