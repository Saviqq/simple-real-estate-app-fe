import React, { Component } from "react";
import PropTypes from "prop-types";
import {
	FormControl,
	FormLabel,
	RadioGroup,
	FormControlLabel,
	Radio
} from "@material-ui/core";

export default class EstateSpecialDealField extends Component {
	render() {
		return (
			<FormControl margin="normal" component="fieldset">
				<FormLabel component="legend"> Special Deal </FormLabel>
				<RadioGroup
					value={this.props.value}
					onChange={event => {
						this.props.handleChange(event, "deal");
					}}
				>
					<FormControlLabel
						value="none"
						control={<Radio color="primary" />}
						label="NONE"
					/>
					<FormControlLabel
						value="hot"
						control={<Radio color="primary" />}
						label="HOT"
					/>
					<FormControlLabel
						value="best"
						control={<Radio color="primary" />}
						label="BEST"
					/>
				</RadioGroup>
			</FormControl>
		);
	}
}

EstateSpecialDealField.propTypes = {
	value: PropTypes.string.isRequired
};
