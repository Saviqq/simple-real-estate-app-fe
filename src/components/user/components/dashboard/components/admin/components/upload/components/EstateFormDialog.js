import React, { Component } from "react";
import PropTypes from "prop-types";
import {
	Dialog,
	DialogTitle,
	DialogContent,
	Grid,
	Typography,
	Button
} from "@material-ui/core";
import { estateFormDialog } from "../cms";

const cms = estateFormDialog;

const sharedProps = {
	style: {
		marginTop: "1em"
	},
	fullWidth: true,
	variant: "contained",
	size: "large",
	color: "primary"
};

export default class EstateFormDialog extends Component {
	render() {
		let buttonLayout;
		if (this.props.uploadSuccess) {
			buttonLayout = (
				<Grid container direction="row" spacing={8}>
					<Grid item xs>
						<Button onClick={() => this.props.resetState()} {...sharedProps}>
							{cms.uploadButton}
						</Button>
					</Grid>
					<Grid item xs>
						<Button {...sharedProps}>{cms.exitButton}</Button>
					</Grid>
				</Grid>
			);
		} else {
			buttonLayout = (
				<Grid>
					<Button onClick={() => this.props.onClose()} {...sharedProps}>
						{cms.backButton}
					</Button>
				</Grid>
			);
		}

		return (
			<Dialog open={this.props.open} onClose={() => this.props.onClose()}>
				<DialogTitle align="center">
					{cms.title} {this.props.uploadSuccess ? cms.success : cms.failure}
				</DialogTitle>

				<DialogContent>
					<Grid>
						<Typography
							align="center"
							variant="subheading"
							style={
								this.props.uploadSuccess
									? { color: "#4BB543" }
									: { color: "#FF0000" }
							}
						>
							{this.props.response}
						</Typography>
					</Grid>

					{buttonLayout}
				</DialogContent>
			</Dialog>
		);
	}
}

EstateFormDialog.propTypes = {
	open: PropTypes.bool.isRequired,
	onClose: PropTypes.func.isRequired,
	response: PropTypes.string.isRequired,
	uploadSuccess: PropTypes.bool.isRequired,
	resetState: PropTypes.func.isRequired
};
