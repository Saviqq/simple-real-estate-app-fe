import { defineMessages } from "react-intl";

export const page = defineMessages({
	header: {
		id: "upload.page.header",
		defaultMessage: "UPLOAD REAL ESTATE"
	}
});

export const form = defineMessages({
	connection: {
		id: "upload.form.connection",
		defaultMessage:
			"Oops! We have a problem on our side :( Please try again later!"
	},
	file: {
		id: "upload.form.file",
		defaultMessage: "You have to pick a thumbnail image for your real estate."
	},
	success: {
		id: "upload.form.success",
		defaultMessage:
			"Real estate uploaded! Would you like to upload another one or return to the admin page?"
	}
});

export const api = {
	createUriApi: "http://localhost:8080/api/createFileUri",
	uploadFileApi: "http://localhost:8080/api/uploadFile",
	uploadEstateApi: "http://localhost:8080/api/estates"
};

export const estateForm = {
	validationError: "You have to fill all of the required fields!"
};

export const estateFields = {
	titleLabel: "Titel",
	planetLabel: "Planet",
	sectorLabel: "Sector",
	typeLabel: "Type",
	roomsLabel: "Rooms",
	garageLabel: "Garage",
	priceLabel: "Price"
};

export const estateUploadButton = {
	buttonLabel: "UPLOAD REAL ESTATE"
};

export const estateFormDialog = {
	title: "Real estate upload was",
	success: "successfull",
	failure: "not successfull",
	backButton: "Back",
	uploadButton: "Upload another",
	exitButton: "Exit"
};
