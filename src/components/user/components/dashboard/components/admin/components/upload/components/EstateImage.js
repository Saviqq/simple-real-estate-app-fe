import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Button } from "@material-ui/core";

class EstateUploadImage extends Component {
	updateUrl = event => {
		var reader = new FileReader();

		reader.onload = e => {
			this.props.changeImgUrl(e.target.result);
		};

		try {
			reader.readAsDataURL(event.target.files[0]);
		} catch (error) {
			return;
		}
	};

	render() {
		return (
			<Grid>
				<input
					style={{ display: "none" }}
					id="upload-image"
					onChange={event => {
						this.updateUrl(event);
						this.props.handleChange(event);
					}}
					type="file"
				/>

				<label htmlFor="upload-image">
					<Button fullWidth variant="outlined" color="primary" component="span">
						{" "}
						CHOOSE THUMBNAIL{" "}
					</Button>
				</label>

				<Grid>
					<label htmlFor="upload-image">
						<img
							style={{ width: "100%", maxHeight: "400px" }}
							src={this.props.imageUrl}
							alt="placeholder"
						/>
					</label>
				</Grid>
			</Grid>
		);
	}
}

EstateUploadImage.propTypes = {
	handleChange: PropTypes.func.isRequired,
	changeImgUrl: PropTypes.func.isRequired,
	imageUrl: PropTypes.string.isRequired
};

export default EstateUploadImage;
