import React, { Component } from "react";
import PropTypes from "prop-types";
import { TextField } from "@material-ui/core";

export default class EstateSelectBox extends Component {
	render() {
		return (
			<TextField
				{...this.props}
				value={this.props.value}
				select
				label={this.props.label}
				onChange={event => {
					this.setState({
						value: event.target.value
					});
					this.props.onChange(event);
				}}
			>
				{this.props.children}
			</TextField>
		);
	}
}

EstateSelectBox.propTypes = {
	label: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};
