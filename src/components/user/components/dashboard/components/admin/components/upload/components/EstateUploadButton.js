import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Button, withStyles } from "@material-ui/core";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { estateUploadButton } from "../cms";

const cms = estateUploadButton;

const styles = theme => ({
	button: {
		margin: theme.spacing.unit
	},
	rightIcon: {
		marginLeft: theme.spacing.unit
	}
});

class EstateUploadButton extends Component {
	render() {
		const { classes } = this.props;

		return (
			<Grid>
				<Button
					variant="contained"
					color="primary"
					className={classes.button}
					fullWidth
					onClick={() => this.props.handleUpload()}
				>
					{cms.buttonLabel}
					<CloudUploadIcon className={classes.rightIcon} />
				</Button>
			</Grid>
		);
	}
}

EstateUploadButton.propTypes = {
	handleUpload: PropTypes.func.isRequired
};

export default withStyles(styles)(EstateUploadButton);
