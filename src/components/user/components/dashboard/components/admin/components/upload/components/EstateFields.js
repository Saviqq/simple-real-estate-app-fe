import React, { Component } from "react";
import PropTypes from "prop-types";
import {
	Grid,
	TextField,
	MenuItem,
	FormControlLabel,
	Checkbox,
	FormHelperText,
	FormControl
} from "@material-ui/core";
import { planets, types, isEmpty } from "../utils";
import EstateSpecialDealField from "./EstateSpecialDealField";
import EstateSelectBox from "./EstateSelectBox";
import { estateFields } from "../cms";

const cms = estateFields;

const sharedProps = {
	margin: "normal",
	variant: "outlined",
	fullWidth: true
};

export default class EstateFields extends Component {
	state = {
		sector: null,
		rooms: null,
		type: null,
		planet: null
	};

	handleRequiredChange = (value, state) => {
		this.setState({
			[state]: value
		});
		this.props.handleChange(value, state);
	};

	showHelperText = state => {
		if (isEmpty(state) && this.props.showFields) {
			return <FormHelperText>This field is required!</FormHelperText>;
		}
	};

	render() {
		return (
			<Grid>
				{/* Title */}
				<TextField
					label={cms.titleLabel}
					value={this.props.estate.title}
					onChange={event =>
						this.props.handleChange(event.target.value, "title")
					}
					{...sharedProps}
				/>

				{/* Planet */}
				<FormControl error {...sharedProps}>
					<EstateSelectBox
						value={this.props.estate.planet}
						label={cms.planetLabel}
						required
						onChange={event =>
							this.handleRequiredChange(event.target.value, "planet")
						}
						{...sharedProps}
					>
						{planets.map(planet => (
							<MenuItem key={planet.key} value={planet.value}>
								{" "}
								{planet.value}{" "}
							</MenuItem>
						))}
					</EstateSelectBox>

					{this.showHelperText(this.state.planet)}
				</FormControl>

				{/* Sector */}
				<FormControl error {...sharedProps}>
					<TextField
						label={cms.sectorLabel}
						required
						value={this.props.estate.sector}
						onChange={event =>
							this.handleRequiredChange(event.target.value, "sector")
						}
						{...sharedProps}
					/>

					{this.showHelperText(this.state.sector)}
				</FormControl>

				{/* Type */}
				<FormControl error {...sharedProps}>
					<EstateSelectBox
						label={cms.typeLabel}
						required
						value={this.props.estate.type}
						onChange={event =>
							this.handleRequiredChange(event.target.value, "type")
						}
						{...sharedProps}
					>
						{types.map(type => (
							<MenuItem key={type.key} value={type.value}>
								{" "}
								{type.value}{" "}
							</MenuItem>
						))}
					</EstateSelectBox>

					{this.showHelperText(this.state.type)}
				</FormControl>

				{/* Rooms */}
				<FormControl error {...sharedProps}>
					<TextField
						label={cms.roomsLabel}
						required
						type="number"
						value={this.props.estate.rooms}
						onChange={event =>
							this.handleRequiredChange(event.target.value, "rooms")
						}
						{...sharedProps}
					/>

					{this.showHelperText(this.state.rooms)}
				</FormControl>

				{/* Garage */}
				<FormControlLabel
					label={cms.garageLabel}
					control={
						<Checkbox
							color="primary"
							checked={this.props.estate.garage}
							onChange={event => {
								this.props.handleChange(event.target.checked, "garage");
							}}
						/>
					}
				/>

				{/* Price */}
				<TextField
					label={cms.priceLabel}
					type="number"
					value={this.props.estate.price}
					onChange={event =>
						this.props.handleChange(event.target.value, "price")
					}
					{...sharedProps}
				/>

				{/* Special */}
				<EstateSpecialDealField
					value={this.props.estate.deal}
					handleChange={(event, state) =>
						this.props.handleChange(event.target.value, state)
					}
				/>
			</Grid>
		);
	}
}

EstateFields.propTypes = {
	handleChange: PropTypes.func.isRequired,
	showFields: PropTypes.bool.isRequired,
	estate: PropTypes.object.isRequired
};
