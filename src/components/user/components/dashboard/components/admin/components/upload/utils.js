export const planets = [
	{ key: 0, value: "Mercury" },
	{ key: 1, value: "Venus" },
	{ key: 2, value: "Earth" },
	{ key: 3, value: "Mars" },
	{ key: 4, value: "Jupiter" },
	{ key: 5, value: "Saturn" },
	{ key: 6, value: "Uranus" },
	{ key: 7, value: "Neptune" },
	{ key: 8, value: "Pluto" }
];

export const types = [{ key: 0, value: "House" }, { key: 1, value: "Flat" }];

export const isEmpty = value => {
	if (value === null || value === undefined || value === "") {
		return true;
	} else return false;
};
