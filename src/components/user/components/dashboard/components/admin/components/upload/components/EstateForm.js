import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";
import EstateFields from "./EstateFields";
import EstateUploadButton from "./EstateUploadButton";
import { isEmpty } from "../utils";
import { estateForm } from "../cms";

const cms = estateForm;

export default class EstateForm extends Component {
	state = {
		reqValues: {
			sector: null,
			rooms: null
		},
		showFields: false
	};

	handleChange = (value, state) => {
		for (var key in this.state.reqValues) {
			if (key === state) {
				var reqValuesToChange = { ...this.state["reqValues"] };
				reqValuesToChange[key] = value;
				this.setState({
					reqValues: reqValuesToChange
				});
			}
		}
		this.props.handleChange(value, state);
	};

	validate = () => {
		this.setState({
			showFields: true
		});

		let isValid = true;
		for (var key in this.state.reqValues) {
			if (isEmpty(this.state.reqValues[key])) {
				isValid = false;
			}
		}

		if (isValid) {
			this.props.handleUpload();
		} else {
			this.props.setDialogStateAndOpenIt(cms.validationError, false);
		}
	};

	render() {
		return (
			<Grid>
				{/* Fields to fill representing Real Estate data */}
				<EstateFields
					estate={this.props.estate}
					showFields={this.state.showFields}
					handleChange={(value, state) => this.handleChange(value, state)}
				/>
				{/* Button for uploading Real Estate on back-end */}
				<EstateUploadButton handleUpload={() => this.validate()} />
			</Grid>
		);
	}
}

EstateForm.propTypes = {
	handleChange: PropTypes.func.isRequired,
	handleUpload: PropTypes.func.isRequired,
	setDialogStateAndOpenIt: PropTypes.func.isRequired,
	estate: PropTypes.object.isRequired
};
