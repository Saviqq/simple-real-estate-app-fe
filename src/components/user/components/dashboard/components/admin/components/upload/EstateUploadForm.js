import React, { Component } from "react";
import axios from "axios";
import { Grid, LinearProgress, TextField } from "@material-ui/core";

import EstateForm from "./components/EstateForm";
import EstateImage from "./components/EstateImage";
import {} from "./containers/EstateImageContainer";
import EstateFormDialog from "./components/EstateFormDialog";
import { form } from "./upload.message";

let uploadFileConfig = {
	headers: { "content-type": "multipart/form-data" }
};

const defaultState = {
	estate: {
		image: {},
		imageUrl: require("./assets/placeholder.jpg"),
		title: "das",
		planet: "",
		sector: "",
		type: "",
		rooms: "",
		garage: false,
		price: "",
		deal: "none"
	},
	response: "",
	showProgress: false,
	modalOpen: false,
	uploadSuccess: false
};

/*
{
    titl state["title"]
}
 * image: {},
    imageUrl: require('./assets/placeholder.jpg'),
    title: "das",
    planet: "",
    sector: "",
    type: "",
    rooms: "",
    garage: false,
    price: "",
    deal: "none",
 */

export default class EstateUploadForm extends Component {
	changeState = (member, value) => {
		this.setState(
			{
				[member]: value
			},
			() => console.log(this.state)
		);
	};

	render() {
		return (
			<Grid>
				<TextField
					margin="normal"
					variant="outlined"
					fullWidth
					onBlur={event => this.changeState("title", event.target.value)}
				/>
			</Grid>
		);
	}
	/*
    resetState = () => {
        this.setState ({
            estate : {
                image: {},
                imageUrl: require('./assets/placeholder.jpg'),
                title: "das",
                planet: "",
                sector: "",
                type: "",
                rooms: "",
                garage: false,
                price: "",
                deal: "none",
            },
            response: "",
            showProgress: false,
            modalOpen: false,
            uploadSuccess: false
        }, () => console.log(this.state));
    }

    imageChosen = (event) => {
        let estate = {...this.state["estate"]};
        estate["image"] = event.target.files[0];

        this.setState({
            estate: estate
        });
    }

    changeImgUrl = (url) => {
        let estate = {...this.state["estate"]};
        estate["imageUrl"] = url;

        this.setState({
            estate: estate
        });
    }

    handleFieldChange = (value, state) => {
        let estate = {...this.state["estate"]};
        estate[state] = value;

        this.setState({
            estate: estate
        });
    }

    changeResponse = (value) => {
        this.setState({
            response: value
        });
    }

    async upload() {
        this.setState({
            showProgress: true
        });

        let formData = new FormData();
        formData.append('file', this.state.estate.image, this.state.estate.image.name);

        let response;
        try {
            response = await this.createFileUri(formData);
        } catch(error) {
            if(error.response.status === 400) {
                this.setDialogMessageAndOpenIt(form., false);
            } else {
                this.setDialogMessageAndOpenIt(cms.noConnection, false);

            }
            return;
        }

        if(response.status === 200) {
            let estate = {...this.state["estate"]};
            estate["imageUrl"] = response.data;
    
            this.setState({
                estate: estate
            }, () => {
                axios.all([this.uploadFile(formData), this.uploadEstate()])
                    .then(axios.spread( () => {
                        this.setDialogMessageAndOpenIt(cms.uploadSuccessfull, true);
                    })).catch((error) => {
                        try {
                            this.setDialogMessageAndOpenIt(error.response.data.message, false);
                        } catch(error) {
                            this.setDialogMessageAndOpenIt(cms.noConnection, false);
                            return;
                        }
                    })
            });
        } else {
            this.setDialogMessageAndOpenIt(response.data.message, false);
        }
    }

    async createFileUri(data) {
        return axios.post(cms.createUriApi, data, uploadFileConfig);  
    }

    async uploadFile(data) {
        return axios.post(cms.uploadFileApi, data, uploadFileConfig);
    }

    
    async uploadEstate() {
        let estate = {...this.state["estate"]};

        console.log(estate);

        return axios.post(cms.uploadEstateApi, {
            title: estate.title,
            imageURL: estate.imageUrl,
            planet: estate.planet,
            sector: estate.sector,
            type: estate.type.toUpperCase(),
            rooms: parseInt(estate.rooms, 16),
            garage: estate.garage,
            price: estate.price,
            dealType: estate.deal.toUpperCase()
        });
    }

    openModal = () => {
        this.setState({
            modalOpen: true
        })
    }

    closeModal = () => {
        this.setState({
            modalOpen: false
        })
    }

    setDialogMessageAndOpenIt = (msg, uploadSuccessfull) => {
        this.changeResponse(msg);
        this.setState({
            showProgress: false,
            uploadSuccess: uploadSuccessfull
        }, () => {
            this.openModal();
        })
    }

    render() {
        return(
            <Grid>
                
                <EstateUploadImage 
                    imageUrl={this.state.estate.imageUrl} 
                    changeImgUrl={(url) => this.changeImgUrl(url)}
                    handleChange={(event) => this.imageChosen(event)} />
                
                <EstateForm  
                    estate={this.state.estate}
                    handleChange={(value, state) => this.handleFieldChange(value, state)}
                    handleUpload={() => this.upload()}
                    setDialogStateAndOpenIt={(msg, uploadSuccessfull) => {this.setDialogMessageAndOpenIt(msg, uploadSuccessfull)}}/>

                <EstateFormDialog 
                    open={this.state.modalOpen} 
                    onClose={() => this.closeModal()}
                    resetState={() => this.resetState()}
                    response={this.state.response}
                    uploadSuccess={this.state.uploadSuccess} />
                {this.state.showProgress ? <LinearProgress /> : <span />}
            </Grid>

        )
    }*/
}
