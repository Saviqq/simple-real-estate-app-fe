import React from "react";
import { FormattedMessage } from "react-intl";
import { Grid, Typography } from "@material-ui/core";

import EstateUploadForm from "./EstateUploadForm";
import { page } from "./upload.message";

const EstateUploadPage = () => (
	<Grid style={{ margin: "3% 0%" }}>
		{/* Page headline */}
		<Typography align="center" variant="display2" gutterBottom>
			<FormattedMessage {...page.header} />
		</Typography>
		{/* Form for uploading new Real Estate */}
		<Grid container justify="center">
			<Grid item lg={3} md={5} sm={8} xs={11}>
				<EstateUploadForm />
			</Grid>
		</Grid>
	</Grid>
);

export default EstateUploadPage;
