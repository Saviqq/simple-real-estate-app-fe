import actions from "./actions";

const logIn = actions.logIn;

const logOut = actions.logOut;

export default {
	logIn,
	logOut
};
