import types from "./types";

export const logIn = () => ({
	type: types.LOG_IN
});

export const logOut = () => ({
	type: types.LOG_OUT
});

export default {
	logIn,
	logOut
};
