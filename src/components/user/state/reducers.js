import { combineReducers } from "redux";

import types from "./types";

/* 
    SHAPE 
    
state: {
    isAdmin: true
    logged: false
}
*/

const logged = (state = false, action) => {
	switch (action.type) {
	case types.LOG_IN:
		return true;
	case types.LOG_OUT:
		return false;
	default:
		return state;
	}
};

const isAdmin = (state = true, action) => {
	switch (action.type) {
	default:
		return state;
	}
};

const userReducers = combineReducers({
	logged,
	isAdmin
});

export default userReducers;
