import React from "react";
import { Grid } from "@material-ui/core";

import EstateList from "../components/estate";

const EstatePage = () => (
	<Grid>
		<EstateList />
	</Grid>
);

export default EstatePage;
