import React from "react";

import UserContainer from "../components/user/UserContainer";

const UserPage = () => <UserContainer />;

export default UserPage;
