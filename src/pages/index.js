import MainPage from "./MainPage";
import AboutPage from "./AboutPage";
import EstatePage from "./EstatePage";
import UserPage from "./UserPage";

export { MainPage, AboutPage, EstatePage, UserPage };