// constants for route strings
export const MAIN_ROUTE = "/main";
export const ABOUT_ROUTE = "/about";
export const ESTATE_ROUTE = "/estate";
export const USER_ROUTE = "/user";

export const USER_LOGIN_ROUTE = "/user-login";
export const USER_DASHBOARD_ROUTE = "/user-dashboard";
