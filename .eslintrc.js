module.exports = {
    "env": {
        "browser": true,
		"es6": true,
		"commonjs": true
    },
    "extends": [
        "eslint:recommended",
		"plugin:react/recommended",
		"prettier"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "parser": "babel-eslint",
    "plugins": [
        "react"
    ],
    "rules": {
        "indent": [
            "error",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ],
        "react/jsx-uses-vars": [
            "error"
		],
		"react/prop-types": 0,
        "no-mixed-spaces-and-tabs": [
            "error",
            "smart-tabs"
        ]
    }
};